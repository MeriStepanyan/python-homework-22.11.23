import math

# 1. Find all of the numbers from 1–100 that have a 6 in them, using list comprehension.
list_6=[x for x in range(1,100) if '6' in str(x)]
print("List which contains elements that have a 6 in them: ",list_6)

#2. Find the common numbers in two lists, using list comprehension(without using a tuple or set) 
# list_a = 1, 2, 3, 4, list_b = 2, 3, 4, 5.

list1=[1,2,3,4]
list2=[2,3,4,5]
commom_list=[x for x in list1 if x in list2 ]
print("The common numbers in two lists: ",commom_list)

# 3.Create a program to invert a dictionary, swapping keys and values.
dict1={
    "name":"Anna",
    "s_name": "Mikayelyan" 
}
print("Dictionary: ",dict1)
reversed_dict = {v: k for k, v in dict1.items()}    #   Version 1
print("Reversed dictionary: ",reversed_dict)

new_dict={}
for i in dict1:
    new_dict[dict1[i]]=i
print("Reversed dictionary: ",new_dict)               #  Version 2

# 4. Create a dictionary where keys are elements from a list and values are their corresponding lengths, using dictionary comprehension.

list1=["name", "surename","age"]
list2=[]
for i in list1:
    list2.append(len(i)) 
new_dict={k: v for k,v in zip(list1, list2)} 
print(new_dict)

# 5. Implement a program that filters a dictionary based on a condition, 
# such as keeping only key-value pairs where the value is greater than a specified threshold.

dict2={
    "age_1": 2,
    "age_2": 40,
    "age_3": 18,
    "age_4": 10,
}
print("Dictionary: ",dict2)
new_dict1 = {k: v for k, v in dict2.items() if v>=18}
print("The new dictionary with values equal or greater than 18: ", new_dict1)

# 6. Given a dictionary with a values list, extract the key whose value has the most unique values.

dict3={
    "a" :[1,2,3,7,9],
    "b" :[2,4,5,6,2,5,7],
    "c" :[5,7,9] 
}
max_un=0
for d in dict3:
    if(len(set(dict3[d]))>max_un):
        max_un=len(set(dict3[d]))
        key=d
print(dict3,"The most unique value has key: ",key)  

# 7. Write a program that takes a list of words and returns a set of characters that are common to all words.
list3=["abc", "bcd","acbd"]
set1=set()
count=0
for j in range(len(list3[0])):
    for i in range(1,len(list3)):
        if list3[0][j] in list3[i]:
            count+=1
    if(count>=2):
        set1.add(list3[0][j])
print(list3, "The characters that are common to all words:",set1) 

# 8. Write a program to merge two dictionaries. If there are common keys, combine their values.
dict1= {"a":1,"b":2}
dict2= {"c":1,"b":3}
new={}
for k_1 in dict1:
    for k_2 in dict2:
        if k_2 in dict1:
           new[k_2]=dict1[k_1]+dict2[k_2]
        else:
           new[k_1]=dict1[k_1]
           new[k_2]=dict2[k_2]
print("The new dict with combined values",new)  

# 9. Given an array of integers, every element appears twice except for one. Find that single one.

array=[1,5,7,1,2,7,5]      
for i in array:
    if(array.count(i)==1):
        print(i)

# 10. Use nested list comprehension to create a matrix (list of lists) of size 3x3 with all elements initialized to zero.  
array= [[0 for j in range(3)] for i in range(3) ] 
print("The 2D array with elements initialized to zero ",array)  

# 11. Given a list of words, create a new list containing only words with a length greater than 5 using 
# list comprehension.

list4=["apple","banana","kivi","blueberry", "lemon","pineapple"]
matrix1 = [i for i in list4 if (len(i)>5) ] 
print(matrix1)

# 12. Write a program that takes a string which contains  lower alphabetic characters and prints "Yes" 
# if we can remove at most one character to make the frequency of each distinct character the same.

str3="aaatppp"  
list3=list(str3)
print(list3)
elem_counter=[list3.count(x) for x in list3 ]
print(elem_counter)
dict3={k:v for k,v in zip(list3,elem_counter)}
print(dict3)
for i in dict3.copy():
    if(dict3[i]==1):
        dict3.pop(i)
        res=len(list(set(list(dict3.values()))))
        if(res==1):
            print("Yes")
            break
else:
    print("No")

# 13. Create a matrix and print the sum of each line's values, using list comprehension.  

matr=[[1,2,3],
[4,5,6],
[7,8,9]]
line_sum=[sum(i) for i in matr] 
print(matr,"New matrix with sum of each line values: ",line_sum)  

# 14. Write a program to print a list which contains the prime numbers of the specified range, 
# using list comprehension. (Use the 'all()' function)  

def is_prime(i):
    if i <= 1:
        return False
    for j in range(2, int(math.sqrt(i)) + 1):
        if i % j == 0:
            return False
    return True

prime_elems=[i for i in range(1,20)  if(is_prime(i))] 
print("The prime numbers list(1,20):",prime_elems)
